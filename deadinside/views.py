from django.shortcuts import render


# Create your views here.
def index(request):
    return render(request, template_name='deadinside/index.html', context={})


def manifesto(request):
    return render(request, template_name='deadinside/manifesto.html', context={})


def links(request):
    return render(request, template_name='deadinside/links.html', context={})


def nginx(request):
    return render(request, template_name='deadinside/nginx.html', context={})


def cloudfare(request):
    return render(request, template_name='deadinside/cloudflare.html', context={})
