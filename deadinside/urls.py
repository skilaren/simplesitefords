from django.urls import path, include
from deadinside.views import index, manifesto, links, nginx, cloudfare


urlpatterns = [
    path('', index, name='index_url'),
    path('manifesto/', manifesto, name='manifesto_url'),
    path('links/', links, name='links_url'),
    path('screen/', cloudfare, name='cloudflare_url'),
    path('nginx/', nginx, name='nginxconf_url'),
]