from django.apps import AppConfig


class DeadinsideConfig(AppConfig):
    name = 'deadinside'
